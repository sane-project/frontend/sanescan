/*  SPDX-License-Identifier: GPL-3.0-or-later

    Copyright (C) 2022  Povilas Kanapickas <povilas@radix.lt>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef SANESCAN_UTIL_IMAGE_INTERNAL_H
#define SANESCAN_UTIL_IMAGE_INTERNAL_H

#include <opencv2/core.hpp>
#include <optional>

namespace sanescan::internal {

struct ImageRotateCenteredParams {
    std::optional<cv::RotateFlags> coarse_rotation;
    double fine_rotation_rad = 0;
};

ImageRotateCenteredParams get_image_rotate_centered_params(double angle_rad);

} // namespace sanescan::internal

#endif // SANESCAN_UTIL_IMAGE_INTERNAL_H
