/*  SPDX-License-Identifier: GPL-3.0-or-later

    Copyright (C) 2021  Povilas Kanapickas <povilas@radix.lt>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include "image.h"
#include "image_internal.h"
#include "util/math.h"
#include <opencv2/imgproc/imgproc.hpp>
#include <iostream>
namespace sanescan {

cv::Mat image_rotate_centered_noflip(const cv::Mat& image, double angle_rad)
{
    if (angle_rad == 0) {
        return image;
    }

    auto height = image.size.p[0];
    auto width = image.size.p[1];

    cv::Mat rotation_mat = cv::getRotationMatrix2D(cv::Point2f(width / 2, height / 2),
                                                   rad_to_deg(angle_rad), 1.0);

    cv::Mat rotated_image;
    cv::warpAffine(image, rotated_image, rotation_mat, image.size(),
                   cv::INTER_LINEAR, cv::BORDER_REPLICATE);
    return rotated_image;
}

namespace internal {

ImageRotateCenteredParams get_image_rotate_centered_params(double angle_rad)
{
    if (angle_rad == 0) {
        return {};
    }

    angle_rad = positive_fmod(angle_rad, deg_to_rad(360));
    double angle_mod90 = near_zero_fmod(angle_rad, deg_to_rad(90));

    ImageRotateCenteredParams params;

    // First we want to rotate whole page which changes the dimensions of the image. We use
    // coarse_rotation (maps to cv::rotate) to rotate 90, 180 or 270 degrees and then
    // fine_rotation_rad (maps to cv::warpAffine) for the final adjustment.
    if (angle_rad - angle_mod90 > deg_to_rad(360 - 45)) {
        angle_rad -= deg_to_rad(360);
    } else if (angle_rad - angle_mod90 > deg_to_rad(270 - 45)) {
        angle_rad -= deg_to_rad(270);
        params.coarse_rotation = cv::ROTATE_90_CLOCKWISE;
    } else if (angle_rad - angle_mod90 > deg_to_rad(180 - 45)) {
        angle_rad -= deg_to_rad(180);
        params.coarse_rotation = cv::ROTATE_180;
    } else if (angle_rad - angle_mod90 > deg_to_rad(90 - 45)) {
        angle_rad -= deg_to_rad(90);
        params.coarse_rotation = cv::ROTATE_90_COUNTERCLOCKWISE;
    }

    params.fine_rotation_rad = angle_rad;
    return params;
}

} // namespace internal

cv::Mat image_rotate_centered(const cv::Mat& image, double angle_rad)
{
    auto params = internal::get_image_rotate_centered_params(angle_rad);
    if (!params.coarse_rotation && params.fine_rotation_rad == 0) {
        return image;
    }

    cv::Mat result = image;
    if (params.coarse_rotation) {
        cv::rotate(image, result, params.coarse_rotation.value());
    }

    return image_rotate_centered_noflip(result, params.fine_rotation_rad);
}

cv::Mat image_color_to_gray(const cv::Mat& image)
{
    cv::Mat result;
    if (image.channels() > 1) {
        cv::cvtColor(image, result, cv::COLOR_BGR2GRAY);
    } else {
        result = image;
    }
    return result;
}

} // namespace sanescan
