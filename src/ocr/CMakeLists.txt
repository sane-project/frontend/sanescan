# SPDX-License-Identifier: GPL-3.0-or-later
#
# Copyright (C) 2021  Povilas Kanapickas <povilas@radix.lt>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

include(FindPkgConfig)
find_package(OpenCV REQUIRED)
pkg_check_modules(tesseract REQUIRED IMPORTED_TARGET tesseract)
pkg_check_modules(libpodofo REQUIRED IMPORTED_TARGET libpodofo)
find_package(Boost COMPONENTS filesystem REQUIRED)

set(SOURCES
    blur_detection.cc
    line_erasure.cc
    ocr_baseline.cc
    ocr_box.cc
    ocr_line.cc
    ocr_paragraph.cc
    ocr_pipeline_run.cc
    ocr_point.cc
    ocr_results_evaluator.cc
    ocr_word.cc
    ocr_utils.cc
    pdf.cc
    pdf_writer.cc
    tesseract.cc
    tesseract_data_utils.cc
    tesseract_renderer.cc
    ../util/image.cc
)

set(PUBLIC_HEADERS
    blur_detection.h
    fwd.h
    hocr.h
    ocr_baseline.h
    ocr_box.h
    ocr_line.h
    ocr_options.h
    ocr_paragraph.h
    ocr_pipeline_run.h
    ocr_point.h
    ocr_results.h
    ocr_results_evaluator.h
    ocr_utils.h
    ocr_word.h
    pdf.h
    tesseract.h
    tesseract_renderer.h
    tesseract_data_utils.h
)

if(SANESCAN_BUILD_HOCR)
    set(SOURCES
        ${SOURCES}
        hocr.cc
    )
    set(PUBLIC_HEADERS
        ${PUBLIC_HEADERS}
        hocr.h
    )
endif()

add_library(sanescanocr ${SOURCES})

target_include_directories(sanescanocr PUBLIC
    $<INSTALL_INTERFACE:${INSTALL_INCLUDE_DIR}>
    $<BUILD_INTERFACE:${CMAKE_SOURCE_DIR}/src>
)

if(SANESCAN_TESSDATA_PATH)
    target_compile_definitions(sanescanocr
        PRIVATE "SANESCAN_TESSDATA_PATH=${SANESCAN_TESSDATA_PATH}")
endif()

target_link_libraries(sanescanocr PUBLIC
    leptonica
    PkgConfig::tesseract
    pugixml
    PkgConfig::libpodofo
    ${OpenCV_LIBS}
    Boost::filesystem
)

install(
    FILES ${PUBLIC_HEADERS}
    DESTINATION ${INSTALL_INCLUDE_DIR}/ocr
)

install(TARGETS sanescanocr)

target_include_directories(sanescanocr PUBLIC
    ${OpenCV_INCLUDE_DIRS}
)
