/*  SPDX-License-Identifier: GPL-3.0-or-later

    Copyright (C) 2022  Povilas Kanapickas <povilas@radix.lt>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include "tesseract_data_utils.h"
#include <boost/process.hpp>
#include <algorithm>

namespace sanescan {

bool TesseractDataSet::has_language(const std::string& language) const
{
    return std::find(languages.begin(), languages.end(), language) != languages.end();
}

std::optional<TesseractDataSet> process_tesseract_dataset(const std::string& path)
{
    TesseractDataSet dataset;
    dataset.path = path;

    if (!boost::filesystem::is_directory(path)) {
        return {};
    }

    for (auto& entry : boost::filesystem::directory_iterator(path)) {
        if (!boost::filesystem::is_regular_file(entry.status())) {
            continue;
        }
        if (entry.path().extension() != ".traineddata") {
            continue;
        }
        dataset.languages.push_back(entry.path().stem().string());
    }

    if (dataset.languages.empty()) {
        return {};
    }

    std::sort(dataset.languages.begin(), dataset.languages.end());
    return dataset;
}

std::vector<TesseractDataSet> get_known_tesseract_datasets()
{
    // TODO: support Windows
    // TODO: change to an asynchronous API

    /* The directory structure of tesseract installation is usually this:
       <prefix>/bin/tesseract
       <prefix>/share/tessdata
    */
    std::vector<std::string> tessdata_candidates;

    auto tess_path = boost::process::search_path("tesseract");
    if (!tess_path.empty()) {
        tessdata_candidates.push_back((tess_path.parent_path() / "share/tessdata").string());
    }

    auto tess_env_path = std::getenv("TESSDATA_PREFIX");
    if (tess_env_path != nullptr) {
        tessdata_candidates.push_back(tess_env_path);
    }

    // Note that this SANESCAN_TESSDATA_PATH is different than what tesseract uses because
    // TESSDATA_PREFIX excludes the final "tessdata" path component.
#ifdef SANESCAN_TESSDATA_PATH
#define QUOTE(s) #s
#define EXPAND_WITH_QUOTES(s) QUOTE(s)
    tessdata_candidates.push_back(EXPAND_WITH_QUOTES(SANESCAN_TESSDATA_PATH));
#endif

    // On Debian-based disributions the packages override the path to
    // /usr/share/tesseract-ocr/4.00/tessdata/ even though tesseract resides in /usr/bin/tesseract.
    tessdata_candidates.push_back("/usr/share/tesseract-ocr/4.00/tessdata/");

    std::vector<TesseractDataSet> valid_datasets;
    for (const auto& candidate_path : tessdata_candidates) {

        auto dataset = process_tesseract_dataset(candidate_path);
        if (dataset) {
            valid_datasets.push_back(*dataset);
        }
    }
    return valid_datasets;
}

} // namespace sanescan
