/*  SPDX-License-Identifier: GPL-3.0-or-later

    Copyright (C) 2022  Povilas Kanapickas <povilas@radix.lt>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef SANESCAN_OCR_TESSERACT_DATA_UTILS_H
#define SANESCAN_OCR_TESSERACT_DATA_UTILS_H

#include <optional>
#include <string>
#include <vector>

namespace sanescan {

struct TesseractDataSet {
    std::string path;
    std::vector<std::string> languages;

    bool has_language(const std::string& language) const;
};

std::optional<TesseractDataSet> process_tesseract_dataset(const std::string& path);
std::vector<TesseractDataSet> get_known_tesseract_datasets();

} // namespace sanescan

#endif // SANESCAN_OCR_TESSERACT_DATA_UTILS_H
