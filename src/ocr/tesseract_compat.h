/*  SPDX-License-Identifier: GPL-3.0-or-later

    Copyright (C) 2021  Povilas Kanapickas <povilas@radix.lt>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef SANESCAN_OCR_TESSERACT_COMPAT_H
#define SANESCAN_OCR_TESSERACT_COMPAT_H

// Tesseract renamed tess_version.h to version.h between 4.x and 5.x, so we can't include that
// header directly.
#include <tesseract/baseapi.h>
#include <tesseract/publictypes.h>

#if TESSERACT_MAJOR_VERSION < 5
// compatibility hack to support tesseract 4.x.
namespace tesseract {

using ::PT_UNKNOWN;
using ::PT_FLOWING_TEXT;
using ::PT_HEADING_TEXT;
using ::PT_PULLOUT_TEXT;
using ::PT_EQUATION;
using ::PT_INLINE_EQUATION;
using ::PT_TABLE;
using ::PT_VERTICAL_TEXT;
using ::PT_CAPTION_TEXT;
using ::PT_FLOWING_IMAGE;
using ::PT_HEADING_IMAGE;
using ::PT_PULLOUT_IMAGE;
using ::PT_HORZ_LINE;
using ::PT_VERT_LINE;
using ::PT_NOISE;
using ::PT_COUNT;

} // namespace tesseract
#endif // TESSERACT_MAJOR_VERSION < 5

#endif // SANESCAN_OCR_TESSERACT_COMPAT_H

