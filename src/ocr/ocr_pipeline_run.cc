/*  SPDX-License-Identifier: GPL-3.0-or-later

    Copyright (C) 2022  Povilas Kanapickas <povilas@radix.lt>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include "line_erasure.h"
#include "ocr_pipeline_run.h"
#include "ocr_results_evaluator.h"
#include "ocr_utils.h"
#include "util/image.h"
#include "tesseract.h"

namespace sanescan {

struct OcrPipelineRun::Data {
    cv::Mat source_image;
    OcrOptions options;
    OcrOptions old_options;
    Mode mode = Mode::FULL;
    std::unique_ptr<TesseractRecognizer> recognizer;
    OcrResults results;
};

OcrPipelineRun::OcrPipelineRun(const cv::Mat& source_image,
                               const OcrOptions& options,
                               const OcrOptions& old_options,
                               const std::optional<OcrResults>& old_results) :
    d_{std::make_unique<Data>()}
{
    d_->source_image = source_image;
    d_->options = options;
    d_->old_options = old_options;
    d_->mode = get_mode(options, old_options, old_results);
    d_->recognizer = std::make_unique<TesseractRecognizer>(d_->options.tessdata_path,
                                                           d_->options.language);
    if (d_->mode == Mode::ONLY_PARAGRAPHS) {
        d_->results = old_results.value();
    }
}

OcrPipelineRun::~OcrPipelineRun() = default;

tesseract::TessBaseAPI& OcrPipelineRun::get_tesseract_api()
{
    return d_->recognizer->get_tesseract_api();
}

void OcrPipelineRun::execute()
{
    try {
        execute_impl();
    } catch (const std::exception& e) {
        d_->results.success = false;
        d_->results.error_text = e.what();
        d_->results.adjusted_image = d_->source_image.clone();
    }
}

OcrResults& OcrPipelineRun::results()
{
    return d_->results;
}

void OcrPipelineRun::execute_impl()
{
    if (d_->mode == Mode::FULL) {
        if (d_->options.language.empty()) {
            throw std::runtime_error("The set language is empty. This should not happen");
        }

        d_->results.paragraphs = d_->recognizer->recognize(d_->source_image);

        // Handle the case when all text within the image is rotated slightly due to the input data
        // scan just being rotated. In such case whole image will be rotated to address the following
        // issues:
        //
        // - Most PDF readers can't select rotated text properly
        // - The OCR accuracy is compromised for rotated text.
        //
        // TODO: Ideally we should detect cases when the text in the source image is legitimately
        // rotated and the rotation is not just the artifact of rotation. In such case the accuracy of
        // OCR will still be improved if rotate the source image just for OCR and then rotate the
        // results back.
        d_->results.adjust_angle = text_rotation_adjustment(d_->results.paragraphs,
                                                            d_->options);
        d_->results.adjusted_image = d_->source_image;

        if (d_->results.adjust_angle != 0) {
            d_->results.adjusted_image = image_rotate_centered(d_->results.adjusted_image,
                                                            d_->results.adjust_angle);
        }
        d_->results.adjusted_image_gray = image_color_to_gray(d_->results.adjusted_image);
        d_->results.adjusted_image_no_lines = d_->results.adjusted_image.clone();
        erase_straight_vh_lines(d_->results.adjusted_image_no_lines,
                                d_->results.adjusted_image_gray,
                                4, 4, 100);

        // FIXME: removal of horizontal and vertical lines requires OCR to be redone. This could
        // potentially be avoided.
        d_->results.paragraphs = d_->recognizer->recognize(d_->results.adjusted_image_no_lines);
        d_->results.blur_data = compute_blur_data(d_->results.adjusted_image_gray);

        if (!d_->options.debug_keep_adjusted_image_no_lines) {
            d_->results.adjusted_image_no_lines = cv::Mat();
        }
    }
    d_->results.adjusted_paragraphs = evaluate_paragraphs(d_->results.paragraphs,
                                                       d_->options.min_word_confidence);
    if (d_->options.blur_detection_coef > 0) {
        d_->results.blurred_words = detect_blur_areas(d_->results.blur_data,
                                                      d_->results.adjusted_paragraphs,
                                                      d_->options.blur_detection_coef);
    }
    d_->recognizer.reset(); // release unneeded memory
}

OcrPipelineRun::Mode OcrPipelineRun::get_mode(const OcrOptions& new_options,
                                              const OcrOptions& old_options,
                                              const std::optional<OcrResults>& old_results)
{
    if (!old_results.has_value()) {
        return Mode::FULL;
    }

    auto new_options_for_full = new_options;
    new_options_for_full.min_word_confidence = 0;
    new_options_for_full.blur_detection_coef = 0;
    auto old_options_for_full = old_options;
    old_options_for_full.min_word_confidence = 0;
    old_options_for_full.blur_detection_coef = 0;

    if (new_options_for_full != old_options_for_full) {
        return Mode::FULL;
    }
    return Mode::ONLY_PARAGRAPHS;
}


} // namespace sanescan
