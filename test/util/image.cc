/*  SPDX-License-Identifier: GPL-3.0-or-later

    Copyright (C) 2022  Povilas Kanapickas <povilas@radix.lt>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include "util/image_internal.h"
#include "util/math.h"
#include <gtest/gtest.h>

namespace sanescan {

struct ImageRotateCenteredTestData {
    double angle_deg = 0;
    std::optional<cv::RotateFlags> expected_coarse_rotation;
    double expected_fine_rotation_deg = 0;
};

TEST(ImageRotateCenteredParams, many_cases)
{
    std::vector<ImageRotateCenteredTestData> tests_data = {
        {0, {}, 0},
        {-50, cv::ROTATE_90_CLOCKWISE, 40},
        {-40, {}, -40},
        {-10, {}, -10},
        {-1, {}, -1},
        {1, {}, 1},
        {44, {}, 44},
        {46, cv::ROTATE_90_COUNTERCLOCKWISE, -44},
        {89, cv::ROTATE_90_COUNTERCLOCKWISE, -1},
        {90, cv::ROTATE_90_COUNTERCLOCKWISE, 0},
        {91, cv::ROTATE_90_COUNTERCLOCKWISE, 1},
        {134, cv::ROTATE_90_COUNTERCLOCKWISE, 44},
        {136, cv::ROTATE_180, -44},
        {179, cv::ROTATE_180, -1},
        {180, cv::ROTATE_180, 0},
        {181, cv::ROTATE_180, 1},
        {224, cv::ROTATE_180, 44},
        {226, cv::ROTATE_90_CLOCKWISE, -44},
        {269, cv::ROTATE_90_CLOCKWISE, -1},
        {270, cv::ROTATE_90_CLOCKWISE, 0},
        {271, cv::ROTATE_90_CLOCKWISE, 1},
        {314, cv::ROTATE_90_CLOCKWISE, 44},
        {316, {}, -44},
        {359, {}, -1},
    };

    for (const auto& test_data : tests_data) {
        auto params = internal::get_image_rotate_centered_params(deg_to_rad(test_data.angle_deg));
        ASSERT_NEAR(rad_to_deg(params.fine_rotation_rad), test_data.expected_fine_rotation_deg,
                    0.001);
        ASSERT_EQ(params.coarse_rotation, test_data.expected_coarse_rotation);
    }
}

} // namespace sanescan
