/*  SPDX-License-Identifier: GPL-3.0-or-later

    Copyright (C) 2021  Povilas Kanapickas <povilas@radix.lt>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include "ocr/ocr_utils.h"
#include "util/math.h"
#include <gtest/gtest.h>

namespace sanescan {

TEST(GetDominantAngle, NoValues)
{
    ASSERT_EQ(get_dominant_angle({}, deg_to_rad(360), 0), std::make_pair(0.0, 0.0));
}

TEST(GetDominantAngle, SingleValue)
{
    ASSERT_EQ(get_dominant_angle({{deg_to_rad(10), 1}}, deg_to_rad(360), deg_to_rad(1)),
              std::make_pair(deg_to_rad(10), 1.0));
    ASSERT_EQ(get_dominant_angle({{deg_to_rad(350), 1}}, deg_to_rad(360), deg_to_rad(1)),
              std::make_pair(deg_to_rad(350), 1.0));
    ASSERT_EQ(get_dominant_angle({{deg_to_rad(360), 1}}, deg_to_rad(360), deg_to_rad(1)),
              std::make_pair(deg_to_rad(0), 1.0));
}

TEST(GetDominantAngle, ManyValuesInSingleWindow)
{
    auto r = get_dominant_angle(
    {
        {deg_to_rad(10), 1},
        {deg_to_rad(11), 2},
        {deg_to_rad(12), 3},
        {deg_to_rad(13), 3},
        {deg_to_rad(14), 2},
        {deg_to_rad(15), 1},
    }, deg_to_rad(360), deg_to_rad(10));

    EXPECT_NEAR(r.first, deg_to_rad(12.5), 1e-6);
    EXPECT_NEAR(r.second, 1.0, 1e-6);
}

TEST(GetDominantAngle, ManyValuesInSingleWindowAcrossZero)
{
    auto r = get_dominant_angle(
    {
        {deg_to_rad(358), 1},
        {deg_to_rad(359), 2},
        {deg_to_rad(360), 3},
        {deg_to_rad(0), 3},
        {deg_to_rad(1), 2},
        {deg_to_rad(2), 1},
    }, deg_to_rad(360), deg_to_rad(10));
    EXPECT_NEAR(r.first, 0.0, 1e-6);
    EXPECT_NEAR(r.second, 1.0, 1e-6);
}

TEST(GetDominantAngle, ManyValuesInSingleWindowAcrossZeroCustomWrapAround)
{
    auto r = get_dominant_angle(
    {
        {deg_to_rad(498), 1},
        {deg_to_rad(499), 2},
        {deg_to_rad(500), 3},
        {deg_to_rad(0), 3},
        {deg_to_rad(1), 2},
        {deg_to_rad(2), 1},
    }, deg_to_rad(100), deg_to_rad(10));
    EXPECT_NEAR(r.first, 0.0, 1e-6);
    EXPECT_NEAR(r.second, 1.0, 1e-6);
}

TEST(GetDominantAngle, ManyValuesInSingleWindowAcrossZeroShiftedNeg)
{
    auto r = get_dominant_angle(
    {
        {deg_to_rad(357), 1},
        {deg_to_rad(358), 2},
        {deg_to_rad(359), 3},
        {deg_to_rad(360), 3},
        {deg_to_rad(0), 2},
        {deg_to_rad(1), 1},
    }, deg_to_rad(360), deg_to_rad(10));
    EXPECT_NEAR(r.first, deg_to_rad((-3*1 - 2*2 - 1*3 + 1*1) / 12.0), 1e-6);
    EXPECT_NEAR(r.second, 1.0, 1e-6);
}

TEST(GetDominantAngle, ManyValuesInSingleWindowAcrossZeroShiftedPos)
{
    auto r = get_dominant_angle(
    {
        {deg_to_rad(359), 1},
        {deg_to_rad(360), 2},
        {deg_to_rad(0), 3},
        {deg_to_rad(1), 3},
        {deg_to_rad(2), 2},
        {deg_to_rad(3), 1},
    }, deg_to_rad(360), deg_to_rad(10));
    EXPECT_NEAR(r.first, deg_to_rad((-1*1 + 1*3 + 2*2 + 3*1) / 12.0), 1e-6);
    EXPECT_NEAR(r.second, 1.0, 1e-6);
}

TEST(GetDominantAngle, ManyValuesInMultipleWindows)
{
    auto r = get_dominant_angle(
    {
        {deg_to_rad(10), 1},
        {deg_to_rad(11), 2},
        {deg_to_rad(12), 2},
        {deg_to_rad(13), 3},
        {deg_to_rad(14), 2},
        {deg_to_rad(15), 1},
        {deg_to_rad(30), 1},
        {deg_to_rad(31), 2},
        {deg_to_rad(32), 3},
        {deg_to_rad(33), 3},
        {deg_to_rad(34), 2},
        {deg_to_rad(35), 1},
    }, deg_to_rad(360), deg_to_rad(10));
    EXPECT_NEAR(r.first, deg_to_rad(32.5), 1e-6);
    EXPECT_NEAR(r.second, 12.0 / 23.0, 1e-6);
}

TEST(GetDominantAngle, BetterValueShiftsOffWorseValues)
{
    auto r = get_dominant_angle(
    {
        {deg_to_rad(10), 1},
        {deg_to_rad(11), 1},
        {deg_to_rad(12), 1},
        {deg_to_rad(13), 1},
        {deg_to_rad(14), 1},
        {deg_to_rad(15), 1},
        {deg_to_rad(16), 1},
        {deg_to_rad(17), 1},
        {deg_to_rad(18), 1},
        {deg_to_rad(19), 1},
        {deg_to_rad(20), 1},
        {deg_to_rad(25), 10},
    }, deg_to_rad(360), deg_to_rad(10));
    EXPECT_NEAR(r.first, deg_to_rad((16 + 17 + 18 + 19 + 20 + 25*10) / 15.0), 1e-6);
    EXPECT_NEAR(r.second, 15.0 / 21.0, 1e-6);
}

namespace {

std::vector<OcrParagraph> build_paragraph_from_directions_and_weights(
        const std::vector<std::pair<double, double>>& angles)
{
    // This matches the implementation of get_all_text_angles()
    if (angles.empty()) {
        return {};
    }

    OcrLine result_line;
    for (const auto& [angle, weight]: angles) {
        OcrWord word;
        word.baseline.angle = angle;
        word.char_boxes.resize(static_cast<std::size_t>(std::round(weight)));
        result_line.words.push_back(word);
    }

    OcrParagraph result_paragraph;
    result_paragraph.lines.push_back(result_line);
    return {result_paragraph};
}

struct RotationAdjustmentAngleTest {
    double rotation_deg = 0;
    double expected_rotation_deg = 0;
};

} // namespace

TEST(TestRotationAdjustment, not_enabled)
{
    OcrOptions options;
    options.fix_page_orientation = false;
    options.fix_text_rotation = false;
    EXPECT_EQ(text_rotation_adjustment(build_paragraph_from_directions_and_weights({{0, 1}}),
                                       options),
              0);
}

TEST(TestRotationAdjustment, only_fix_page_orientation)
{
    std::vector<RotationAdjustmentAngleTest> tests_data = {
        { 0, 0 },
        { -4, 0 },
        { 356, 0 },
        { 4, 0 },
        { 90, 90 },
        { 86, 90 },
        { 94, 90 },
        { 180, 180 },
        { 176, 180 },
        { 184, 180 },
        { 270, 270 },
        { 266, 270 },
        { 274, 270 },
        // too large differences
        { -6, 0 },
        { 354, 0 },
        { 6, 0 },
        { 84, 0 },
        { 96, 0 },
        { 174, 0 },
        { 186, 0 },
        { 264, 0 },
        { 276, 0 },
    };

    for (auto test_data : tests_data) {
        auto paragraphs = build_paragraph_from_directions_and_weights({
            {deg_to_rad(test_data.rotation_deg), 5}
        });

        OcrOptions options;
        options.fix_page_orientation = true;
        options.fix_page_orientation_max_angle_diff = deg_to_rad(5);
        options.fix_text_rotation = false;

        EXPECT_NEAR(positive_fmod(text_rotation_adjustment(paragraphs, options), deg_to_rad(360)),
                    positive_fmod(deg_to_rad(test_data.expected_rotation_deg), deg_to_rad(360)),
                    0.0001);
    }
}

TEST(TestRotationAdjustment, only_fix_text_rotation)
{
    std::vector<RotationAdjustmentAngleTest> tests_data = {
        { 0, 0 },
        { -4, -4 },
        { 356, -4 },
        { 4, 4 },
        { 90, 0 },
        { 86, -4 },
        { 94, 4 },
        { 180, 0 },
        { 176, -4 },
        { 184, 4 },
        { 270, 0 },
        { 266, -4 },
        { 274, 4 },
    };

    for (auto test_data : tests_data) {
        auto paragraphs = build_paragraph_from_directions_and_weights({
            {deg_to_rad(test_data.rotation_deg), 5}
        });

        OcrOptions options;
        options.fix_page_orientation = false;
        options.fix_text_rotation = true;
        options.fix_text_rotation_max_angle_diff = deg_to_rad(5);

        EXPECT_NEAR(positive_fmod(text_rotation_adjustment(paragraphs, options), deg_to_rad(360)),
                    positive_fmod(deg_to_rad(test_data.expected_rotation_deg), deg_to_rad(360)),
                    0.0001);
    }
}

TEST(TestRotationAdjustment, both_fix_page_orientation_and_text_rotation)
{
    std::vector<RotationAdjustmentAngleTest> tests_data = {
        { 0, 0 },
        { -4, -4 },
        { 356, -4 },
        { 4, 4 },
        { 90, 90 },
        { 86, 86 },
        { 94, 94 },
        { 180, 180 },
        { 176, 176 },
        { 184, 184 },
        { 270, 270 },
        { 266, 266 },
        { 274, 274 },
        // too large differences
        { -6, 0 },
        { 354, 0 },
        { 6, 0 },
        { 84, 0 },
        { 96, 0 },
        { 174, 0 },
        { 186, 0 },
        { 264, 0 },
        { 276, 0 },
    };

    for (auto test_data : tests_data) {
        auto paragraphs = build_paragraph_from_directions_and_weights({
            {deg_to_rad(test_data.rotation_deg), 5}
        });

        OcrOptions options;
        options.fix_page_orientation = true;
        options.fix_page_orientation_max_angle_diff = deg_to_rad(5);
        options.fix_text_rotation = true;
        options.fix_text_rotation_max_angle_diff = deg_to_rad(5);

        EXPECT_NEAR(positive_fmod(text_rotation_adjustment(paragraphs, options), deg_to_rad(360)),
                    positive_fmod(deg_to_rad(test_data.expected_rotation_deg), deg_to_rad(360)),
                    0.0001);
    }
}

} // namespace sanescan
