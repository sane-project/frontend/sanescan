{
  description = "A scanning and OCR application";
  inputs = {
    utils.url = "github:numtide/flake-utils";
  };

  outputs = { self, utils, nixpkgs }:
    let
      systems = with utils.lib.system; [
        aarch64-linux
        i686-linux
        x86_64-linux
      ];
    in
    utils.lib.eachSystem systems (system:
      let
        pkgs = import nixpkgs { inherit system; };
        tesseract5_patched = pkgs.tesseract5.overrideAttrs (old: {
          version = "5.1.1";
          src = pkgs.fetchFromGitHub {
            owner = "p12tic";
            repo = "tesseract";
            rev = "51a3398a3c1448fdedc9dc7de7790639d877ca7d";
            sha256 = "5H80v6rlKBcAuPBFcYPDtp4TagSzR59Qh3PZjjmdbHw=";
          };
        });
        sanescan = (with pkgs; stdenv.mkDerivation rec {
          pname = "sanescan";
          version = "0.0.1";
          src = ./.;
          nativeBuildInputs = [
            extra-cmake-modules
            pkg-config
            ninja
            libsForQt5.qt5.wrapQtAppsHook
            cmake
          ];
          buildInputs = [
            boost
            gtest
            leptonica
            opencv
            podofo
            poppler
            pugixml
            sane-backends
            libsForQt5.qt5.qtbase
            libsForQt5.qt5.qttools
            libsForQt5.qt5.qtx11extras
            tesseract5_patched
          ];
          cmakeFlags = [
            "-DSANESCAN_TESSDATA_PATH=${tesseract5_patched.out}/share/tessdata"
          ];
          dontStrip = true;
          preFixup = ''
            makeWrapperArgs+=("''${qtWrapperArgs[@]}")
          '';
          checkPhase = "test/unittest";
          installPhase = ''
            mkdir -p $out/bin
            mv src/cli/sanescancli $out/bin
            mv src/gui/sanescan $out/bin
          '';
        });
      in
      rec {
        packages.sanescan = sanescan;
        packages.default = sanescan;
        apps.sanescan = {
          type = "app";
          program = "${self.packages.${system}.sanescan}/bin/sanescan";
        };
        apps.sanescancli = {
          type = "app";
          program = "${self.packages.${system}.sanescan}/bin/sanescancli";
        };
      });
}
